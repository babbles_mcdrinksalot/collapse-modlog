# Overview
slack-modlog is a service that broadcasts important events from a subreddit's moderator log to a Slack webhook.

# Installation
`git clone https://gitlab.com/babbles_mcdrinksalot/slack-modlog`

# Configuration
1. Make a copy of `config.json.example` and call it `config.json`
1. Use [`reddit-oauth-helper`](https://github.com/not-an-aardvark/reddit-oauth-helper) to create a new reddit app and obtain a `client_id`, `client_secret` and `refresh_token`
1. Put `client_id`, `client_secret` and `refresh_token` in `config.json` along with the `subreddit` you'd like the service to monitor
1. [Create an incoming webhook in Slack](https://api.slack.com/incoming-webhooks)
1. Put the url for your `slack_webhook_url` into `config.json`

# Running
`node slack-modlog/index.js`