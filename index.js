#!/usr/bin/env node
'use strict';

const snoowrap = require('snoowrap');
const request = require('request');

const config = require('./config.json');
const modlog = require('./modlog');

if (require.main === module) {
	let m = new modlog(config, snoowrap, request);
	console.log(`Begin polling every ${config.polling_interval / 1000} seconds...`);
	m.mainloop();
} else {
	module.exports = modlog;
}