'use strict';

const utils = require('../utils');

module.exports = function(config, snoowrap, request) {
	const webhook_request = request.defaults({
		url: config.slack_webhook_url,
		method: 'POST'
	});

	const r = new snoowrap(config);

	var last_modact_time = 0;

	var me = this;

	this.broadcast = function(text) {
		console.log(`[BROADCAST] ${text}`);

		webhook_request({
			json: { text: text }
		});
	};

	this.process_listings = function (listings) {
		listings.reverse().forEach(function(modlog) {
			if (modlog.created_utc > last_modact_time) {
				config.debug && console.log(`[DEBUG] considering modact ${modlog.action} by ${modlog.mod} (${modlog.created_utc})`);

				switch (modlog.action) {
					case 'banuser': {
						let details = '';

						if (modlog.details == 'permanent' || modlog.details == 'changed to permanent') {
							details = ' permanently';
						} else {
							details = ` for ${modlog.details}`;
						}

						me.broadcast(`${utils.get_userlink(modlog.mod)} banned user ${utils.get_userlink(modlog.target_author)} ${details}.  Reason: ${modlog.description}`);
						break;
					}
					case 'unbanuser': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} unbanned user ${utils.get_userlink(modlog.target_author)}`);
						break;
					}
					case 'removelink': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} removed link ${utils.get_postlink(modlog.target_title, modlog.target_permalink)}`);
						break;
					}
					case 'removecomment': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} removed comment by ${utils.get_userlink(modlog.target_author)}.  ${utils.get_postlink('Link', modlog.target_permalink)}`);
						break;
					}
					case 'invitemoderator': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} invited ${utils.get_userlink(modlog.target_author)} to be a moderator`);
						break;
					}
					case 'distinguish': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} distinguished ${utils.get_postlink('a comment', modlog.target_permalink)}`);
						break;
					}
					case 'wikirevise': {
						me.broadcast(`Wiki ${modlog.details} by ${utils.get_userlink(modlog.mod)}.  Description: ${modlog.description}`);
						break;
					}
					case 'sticky': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} stickied ${utils.get_postlink('a comment or post', modlog.target_permalink)}`);
						break;
					}
					case 'unsticky': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} unstickied ${utils.get_postlink('a comment or post', modlog.target_permalink)}`);
						break;
					}
					case 'lock': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} locked thread ${utils.get_postlink(modlog.target_title, modlog.target_permalink)}`);
						break;
					}
					case 'unlock': {
						me.broadcast(`${utils.get_userlink(modlog.mod)} unlocked thread ${utils.get_postlink(modlog.target_title, modlog.target_permalink)}`);
						break;
					}
				}

				last_modact_time = modlog.created_utc;
			}
		});
	};

	this.mainloop = function() {
		if (last_modact_time === 0) {
			// initialize
			r.get_subreddit(config.subreddit).get_moderation_log({ limit: 1 }).then(function(modlog) {
				last_modact_time = modlog[0].created_utc;
			})
			.catch(function(error) {
				console.log(`[ERROR] ${error.name} while initializing modlog loop.  Message: ${error.message}`);
			})
			.finally(function() {
				setTimeout(me.mainloop, config.polling_interval);
			});
		} else {
			// normal operations
			r.get_subreddit(config.subreddit).get_moderation_log({ limit: 10 }).then(me.process_listings).catch(function(error) {
				console.log(`[ERROR] ${error.name}.  Message: ${error.message}`);
			}).finally(function() {
				setTimeout(me.mainloop, config.polling_interval);
			});
		}
	};
};