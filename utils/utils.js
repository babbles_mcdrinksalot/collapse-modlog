'use strict';

module.exports = {
	get_userlink: function(username) {
		if (username == '[deleted]') return username;
		return `<https://www.reddit.com/u/${username}|/u/${username}>`;
	},
	get_postlink: function(title, permalink) {
		return `<https://www.reddit.com${permalink}|${title}>`;
	}
};